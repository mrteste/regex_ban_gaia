import sys
sys.path.append('~Documents/Codes/Tests/Python/gaia_insee/')
import pandas as pd
import numpy as np
import timeit

ban_10000 = pd.read_csv("ban_10000.csv",encoding = "utf-8")
#print(ban_10000.columns)
noms_voie = ban_10000['nom_voie'].values.tolist()
np_noms_voie = np.array(noms_voie, dtype = str)

liste = timeit.timeit('[a.lower() for a in noms_voie]', globals=globals(), number=1000)
nparray = timeit.timeit('np.char.lower(np_noms_voie)', globals=globals(), number=1000)
print(f'comprehensive list {liste}')
print(f'numpy array {nparray}')

# La liste par compréhension est presque 4 fois plus rapide que numpy pour passer tous les caratères en minuscules.