import cython
import numpy as np
#cimport numpy as np
import re

# f1 : double boucle avec listes - python-like
@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing
def f1(MaVariable: list, regex: list, dilat: list) -> list:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    for i in range(n): 
        for j in range(m): 
            MaVariableOut[i] = re.sub(regex[j],dilat[j],MaVariableOut[i])
    return MaVariableOut

# f2 : double boucle avec listes - cython-like
@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing
cpdef list f2(list MaVariable,list regex, list dilat):
    cdef int n = len(MaVariable)
    cdef int m = len(regex)
    cdef list MaVariableOut = [a.lower().strip() for a in MaVariable]
    for i in range(n): 
        for j in range(m): 
            MaVariableOut[i] = re.sub(regex[j],dilat[j],MaVariableOut[i])
    return MaVariableOut

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing
cpdef list f3(list MaVariable,list regex, list dilat):
    cdef int n = len(MaVariable)
    cdef int m = len(regex)
    cdef list MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = [re.compile(reg) for reg in regex]
    for i in range(n):
        for j in range(m):
            MaVariableOut[i] = patterns[j].sub(dilat[j],MaVariableOut[i])
    return MaVariableOut


# f3 : double boucle avec array - cython-like
# cpdef list f3(char [:] MaVariable,char [:] regex,char[:] dilat):
#     cdef char [:] MaVariableOut = np.strip(np.char.lower(MaVariable))
#     cdef int n = len(MaVariable)
#     cdef int m = len(regex)
#     for i in range(n): 
#         for j in range(m): 
#             MaVariableOut[i] = re.sub(regex[j],dilat[j],MaVariableOut[i])
#     return MaVariableOut

#f4 : proposition Benoît
# ctypedef np.str_t DTYPE_t
# #@njit(parallel=True, fastmath=True)
# def f4(str MaVariable, np.ndarray[DTYPE_t, ndim=1] regex, np.ndarray[DTYPE_t, ndim=1] dilat):
#     cdef str x = MaVariable.lower().strip()
#     cdef int n = regex.shape[0]
#     for i in range(n):
#         x = re.sub(regex[i],dilat[i],x)
#     return x

# f4 : vectorisation de la fonction re.sub
@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing
def f4(MaVariable: list, regex: list, dilat: list)  -> np.array:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = [re.compile(reg) for reg in regex]
    def sub_str(pattern, dilat_char, string_arg):
        return pattern.sub(dilat_char, string_arg)
    sub_vec = np.vectorize(sub_str, excluded=['dilat_char'])
    for j in range(m):
        MaVariableOut = sub_vec(patterns[j], dilat[j], MaVariableOut)
    return MaVariableOut

# f5: avec un dictionnaire
# Nécessite un dictionnaire : clé = chaîne à remplacer, valeur : chaîne remplaçante
@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing
def f5(MaVariable: list, regex: dict) -> list:
    # n = len(MaVariable)
    # m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = re.compile(r'(?<!\w)(' + '|'.join(re.escape(key) for key in regex.keys()) + r')(?!\w)')
    return [patterns.sub(lambda x: regex[x.group(0)], text) for text in MaVariableOut]