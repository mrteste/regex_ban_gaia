import sys
sys.path.append('~Documents/Codes/Tests/Python/regex_ban_gaia/')
import pandas as pd
import numpy as np
import time
import fonctions_python as funpy 
import fonctions_pythonx as funpyx

dnv = pd.read_csv("DilatNomVoieNoRegex.csv",encoding = "utf-8")
regex_dnv = dnv['regex'].values.tolist()
dilat_dnv = dnv['dilat'].values.tolist()

#regex_dnv_par = list(map(lambda x: re.sub('\[','(',re.sub('\]',')',x)),regex_dnv))

ban = pd.read_csv("ban_100000.csv",encoding = "utf-8")
#print(ban_10000.columns)
noms_voie = ban['nom_voie'].values.tolist()

np_regex_dnv = np.array(dnv['regex'].values, dtype = str)
np_dilat_dnv = np.array(dnv['dilat'].values, dtype = str)
np_noms_voie = np.array(noms_voie, dtype = str)

dico_dnv = dict(zip(regex_dnv, dilat_dnv))

start = time.process_time()
noms_voie_corr = funpy.f1(noms_voie, regex_dnv, dilat_dnv)
print( 'fonction {}, temps {}'.format('funpy.f1', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f12(np_noms_voie, np_regex_dnv, np_dilat_dnv)
print( 'fonction {}, temps {}'.format('funpy.f12', time.process_time() - start))

# start = time.process_time()
# noms_voie_corr = funpy.f13(noms_voie, regex_dnv, dilat_dnv)
# print( 'fonction {}, temps {}'.format('funpy.f13', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f14(noms_voie, regex_dnv, dilat_dnv)
print( 'fonction {}, temps {}'.format('funpy.f14', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f15(noms_voie, regex_dnv, dilat_dnv)
print( 'fonction {}, temps {}'.format('funpy.f15', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f16(noms_voie, dico_dnv)
print( 'fonction {}, temps {}'.format('funpy.f16', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f1(noms_voie, regex_dnv, dilat_dnv)
print('fonction {}, temps {}'.format('funpyx.f1', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f2(noms_voie, regex_dnv, dilat_dnv)
print('fonction {}, temps {}'.format('funpyx.f2', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f3(noms_voie, regex_dnv, dilat_dnv)
print('fonction {}, temps {}'.format('funpyx.f3', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f4(noms_voie, regex_dnv, dilat_dnv)
print('fonction {}, temps {}'.format('funpyx.f4', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f5(noms_voie, dico_dnv)
print('fonction {}, temps {}'.format('funpyx.f5', time.process_time() - start))
