import sys
sys.path.append('~Documents/Codes/Tests/Python/regex_ban_gaia/')
import pandas as pd
import numpy as np
import time
import fonctions_python as funpy 
import fonctions_pythonx as funpyx

dtv = pd.read_csv("DilatTypeVoie.csv",encoding = "utf-8")
regex_dtv = dtv['regex'].values.tolist()
dilat_dtv = dtv['dilat'].values.tolist()

#regex_dtv_par = list(map(lambda x: re.sub('\[','(',re.sub('\]',')',x)),regex_dtv))

ban_10000 = pd.read_csv("ban_10000.csv",encoding = "utf-8")
#print(ban_10000.columns)
noms_voie = ban_10000['nom_voie'].values.tolist()

np_regex_dtv = np.array(dtv['regex'].values, dtype = str)
np_dilat_dtv = np.array(dtv['dilat'].values, dtype = str)
np_noms_voie = np.array(noms_voie, dtype = str)

dico_dtv = dict(zip(regex_dtv, dilat_dtv))

start = time.process_time()
noms_voie_corr = funpy.f1(noms_voie, regex_dtv, dilat_dtv)
print( 'fonction {}, temps {}'.format('funpy.f1', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f12(np_noms_voie, np_regex_dtv, np_dilat_dtv)
print( 'fonction {}, temps {}'.format('funpy.f12', time.process_time() - start))

# start = time.process_time()
# noms_voie_corr = funpy.f13(noms_voie, regex_dtv, dilat_dtv)
# print( 'fonction {}, temps {}'.format('funpy.f13', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f14(noms_voie, regex_dtv, dilat_dtv)
print( 'fonction {}, temps {}'.format('funpy.f14', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpy.f15(noms_voie, regex_dtv, dilat_dtv)
print( 'fonction {}, temps {}'.format('funpy.f15', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f1(noms_voie, regex_dtv, dilat_dtv)
print('fonction {}, temps {}'.format('funpyx.f1', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f2(noms_voie, regex_dtv, dilat_dtv)
print('fonction {}, temps {}'.format('funpyx.f2', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f3(noms_voie, regex_dtv, dilat_dtv)
print('fonction {}, temps {}'.format('funpyx.f3', time.process_time() - start))

start = time.process_time()
noms_voie_corr = funpyx.f4(noms_voie, regex_dtv, dilat_dtv)
print('fonction {}, temps {}'.format('funpyx.f4', time.process_time() - start))
