import numpy as np
import re

# f1 : double boucle avec listes
def f1(MaVariable: list, regex: list, dilat: list) -> list:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    for i in range(n): 
        for j in range(m): 
            MaVariableOut[i] = re.sub(regex[j],dilat[j],MaVariableOut[i])
    return MaVariableOut


# f12 : double boucle avec np arrays
def f12(MaVariable: np.array, regex: np.array, dilat: np.array) -> np.array:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = np.char.lower(MaVariable)
    for i in range(n): 
        for j in range(m): 
            MaVariableOut[i] = re.sub(regex[j],dilat[j],MaVariableOut[i])
    return MaVariableOut


# f13 : double boucle avec list comprehension
# problème résultat
# def f13(MaVariable: list, regex: list, dilat: list) -> list:
#     n = len(MaVariable)
#     m = len(regex)
#     MaVariableOut = [a.lower().strip() for a in MaVariable]
#     for i in range(n): 
#         MaVariableOut[i] = [re.sub(regex[j],dilat[j],MaVariable[i]) for j in range(m)]
#     return MaVariableOut


# f14 : double boucle avec list comprehension
def f14(MaVariable: list, regex: list, dilat: list) -> list:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = [re.compile(reg) for reg in regex]
    for i in range(n): 
        for j in range(m):
            MaVariableOut[i] = patterns[j].sub(dilat[j],MaVariableOut[i])
    return MaVariableOut

# f15 : vectorisation de la fonction re.sub
def f15(MaVariable: list, regex: list, dilat: list) -> np.array:
    n = len(MaVariable)
    m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = [re.compile(reg) for reg in regex]
    def sub_str(pattern, dilat_char, string_arg):
        return pattern.sub(dilat_char, string_arg)
    sub_vec = np.vectorize(sub_str, excluded=['dilat_char'])
    for j in range(m):
        MaVariableOut = sub_vec(patterns[j], dilat[j], MaVariableOut)
    return MaVariableOut

# f16 : avec un dictionnaire
# Nécessite un dictionnaire : clé = chaîne à remplacer, valeur : chaîne remplaçante
def f16(MaVariable: list, regex: dict) -> list:
    # n = len(MaVariable)
    # m = len(regex)
    MaVariableOut = [a.lower().strip() for a in MaVariable]
    patterns = re.compile(r'(?<!\w)(' + '|'.join(re.escape(key) for key in regex.keys()) + r')(?!\w)')
    return [patterns.sub(lambda x: regex[x.group(0)], text) for text in MaVariableOut]