# README

## Les fichiers :
- `fonctions_python.py` : fonctions en python pur, en testant les différentes
possibilités offertes par le langage (compréhension de liste, dictionnaires, numpy.array);
- `fonctions_pythons.pyx` : fonctions équivalentes passées en cython;
- `main.py` : test des différentes fonctions avec la regex des types de voie (la
fonction utilisant des dictionnaires n'est pas applicable ici);
- `main_dnv.py` : test des fonctions y compris la fonction avec la solution 
utilisant un dictionnaire (nécessite le fichier csv DilatNomVoieNoRegex.csv).

Les deux programmes `main.py` et `main_dnv.pyx` comparent les temps d'exécution des
fonctions.

## Enseignements

A travers les fonctions testées, ce qui permet des gains significatifs :
- l'utilisation de la fonction `re.compile` en amont permet **de diviser par deux** 
les temps de calcul - la fonction compile les expressions régulières en amont et 
ce qui limite son appel implicite par `re.sub` sur chaque itération;
- le passage d'une fonction python en cython ne permet qu'une **amélioration limitée**
des performances (10% environ);
- l'utilisation quand c'est possible des listes par comprehension mène à un petit 
gain également (10% environ);

Autres tentatives pour accélérer l'exécution qui se sont avérées décevantes :
- la vectorisation de `re.sub` avec la fonction `np.vectorize()`;
- l'utilisation de numpy.arrays.


## La meilleure option trouvée

On réussit à **diviser par 10** les temps d'exécution en utilisant un dictionnaire 
associant valeurs à remplacer et valeurs remplaçantes. Les clés ne doivent par contre 
pas contenir les caractères spéciaux d'une expression régulière telle que '(','[','|', etc.
Pour les types de voie, on a pu appliquer cette technique car il était simple de constituer
un tableau sans formalisme d'expressions régulières dans la colonne regex (voir fichier
DilatTypeVoieNoRegex.csv).

Pour cette solution, voir la fonction python `f16` et la fonction cython correspondante `f5`.



