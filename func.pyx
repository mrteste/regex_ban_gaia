import cython
import sys
#sys.path.append('X:\\HAB-GAIA\\GAIA_Python\\Python')
import pandas as pd
import re
import numpy as np
cimport numpy as np
#dtv = pd.read_csv("X:/HAB-GAIA/Test/DilatTypeVoie.csv",encoding = "utf-8")
#dnv = pd.read_csv("X:/HAB-GAIA/Test/DilatNomVoie.csv",encoding = "utf-8")
#drv = pd.DataFrame(np.array([["\\bb\\b|\\bbis?\\b", "bis"],["\\bt\\b", "ter"],["\\bqua\\b", "quater"],["\\bqui\\b", "quinquies"]]),
 #                  columns=['regex', 'dilat'])
                   
                   
dtv = pd.read_csv("Test/DilatTypeVoie.csv",encoding = "utf-8")
dnv = pd.read_csv("DilatNomVoie.csv",encoding = "utf-8")
drv = pd.DataFrame(np.array([["\\bb\\b|\\bbis?\\b", "bis"],["\\bt\\b", "ter"],["\\bqua\\b", "quater"],["\\bqui\\b", "quinquies"]]),
                   columns=['regex', 'dilat'])
regex_dtv = np.array(dtv['regex'].values).astype(str)
dilat_dtv = np.array(dtv['dilat'].values).astype(str)
regex_dnv = np.array(dnv['regex'].values).astype(str)
dilat_dnv = np.array(dnv['dilat'].values).astype(str)
regex_drv = np.array(drv['regex'].values).astype(str)
dilat_drv = np.array(drv['dilat'].values).astype(str)
DTYPE = np.str


#ctypedef np.str_t DTYPE_t

#@njit(parallel=True, fastmath=True)
#def DilatTypeVoie(str MaVariable, np.ndarray[DTYPE_t, ndim=1] regex, np.ndarray[DTYPE_t, ndim=1] dilat):
 #   cdef str x = MaVariable.lower().strip()
 #   cdef int n = regex.shape[0]
 #   for i in range(n):
 #       x = re.sub(regex[i],dilat[i],x)
 #   return x