from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("fonctions_pythonx.pyx")
)

# A exécuter dans le terminal :
#python setup.py build_ext --inplace