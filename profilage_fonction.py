import cProfile
import sys
sys.path.append('~Documents/Codes/Tests/Python/regex_ban_gaia/')
import pandas as pd
# import numpy as np
import re
import fonctions_python as funpy 
# import fonctions_pythonx as funpyx

dtv = pd.read_csv("DilatTypeVoie.csv",encoding = "utf-8")
regex_dtv = dtv['regex'].values.tolist()
def remplace_crochets(s):
    dict_pattern = {
        '[':'(',
        ']':')'
    }
    pattern = re.compile('|'.join(dict_pattern.keys()))
    return pattern.sub(lambda x: dict_pattern[x.group()], s)
regex_dtv_par = map(remplace_crochets, regex_dtv)
dilat_dtv = dtv['dilat'].values.tolist()

ban_10000 = pd.read_csv("ban_10000.csv",encoding = "utf-8")
#print(ban_10000.columns)
noms_voie = ban_10000['nom_voie'].values.tolist()

with cProfile.Profile() as pr:
    noms_voie = funpy.f1(noms_voie, regex_dtv, dilat_dtv)
pr.print_stats()
